
FlowRouter.route('/', {
  name: 'index',
  action(params, queryParams) {
    console.log('index 1')
      BlazeLayout.setRoot('body');
      BlazeLayout.render('App_body', {main: 'home'});
  }
});

FlowRouter.route('/login', {
  name: 'login',
  action() {
    if (Meteor.userId()) {
      console.log('guest 1')
      FlowRouter.go("/");
    } else {
      BlazeLayout.setRoot('body');
      BlazeLayout.render('App_body', {main: 'login'});
    }
  }
})

FlowRouter.route('/register', {
  name: 'register',
  action() {
    if (Meteor.userId()) {
      console.log('guest 1')
      FlowRouter.go("/");
    } else {
      BlazeLayout.setRoot('body');
      BlazeLayout.render('App_body', {main: 'register'});
    }
  }
})

FlowRouter.route('/logout', {
  name: 'logout',
  action() {
    Meteor.logout(() => {
      FlowRouter.go('/')
    })
  }
})
