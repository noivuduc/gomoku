import { Mongo } from 'meteor/mongo'
import { Session } from 'meteor/session'
import { Constant } from '../common/constant.js'

export const GameBoards = new Mongo.Collection('gameboard')

Meteor.methods({
  /**
   * Create new Game board
   * @param {string} name 
   * @param {{name, id}} owner 
   */
  createGame(name) {
    let gameboard = {
      name,
      type: {
        player1: 'X',
        player2: 'O'
      },
      player1: this.userId,
      player1Name: Meteor.user().username,
      player2: null,
      player2Name: null,
      lastPlay: null,
      starter: this.userId,
      winnerId: null,
      winnerName: null,
      createdAt: new Date(),
      table: grid(),
      status: 'waiting',
      swap2: { // swap2 rule
        step: 0,
        action: 0 // 0: player1 playing, 1: player2 accepted, 2: swap, 3: player2 put 2 more stones
      }
    }

    GameBoards.insert(gameboard, (err) => {
      if (err) {
        throw new Error(err.reason)
      }
    })
  },

  /**
   * Let player 2 join a selected board
   * @param {string} id 
   */
  join(id) {
    let board = GameBoards.find({ _id: id }).fetch()[0]
    let join = {
      player1: board.player1 ? board.player1 : this.userId,
      player1Name: board.player1Name ? board.player1Name : Meteor.user().username,
      player2: board.player2 ? board.player2 : this.userId,
      player2Name: board.player2Name ? board.player2Name : Meteor.user().username,
      status: 'playing'
    }
    GameBoards.update(id, {
      $set: join
    }, err => {
      if (err) {
        throw new Error(err.reason)
      }
    })
  },

  /**
   * Update moving on the board
   * @param {Int} x 
   * @param {Int} y 
   * @param {String} type 
   */
  updateMove(x, y) {
    x = Number(x)
    y = Number(y)
    let gameboard = GameBoards.find({ $or: [{ player1: Meteor.userId() }, { player2: Meteor.userId() }] }).fetch()[0]
    if (gameboard.status == 'finished') {
      return false
    }
    if (!gameboard.player2 || !gameboard.player1) {
      throw new Meteor.Error('Waiting for your opponent')
    }

    if (gameboard.starter != this.userId && !gameboard.lastPlay) {
      throw new Meteor.Error('Not your turn')
    }
    //If player1 added 3 stones already, player2 has to select 1 in 3 options
    if (gameboard.swap2.step == 3 && gameboard.swap2.action == Constant.SWAP2.PLAYING && gameboard.lastPlay != this.userId) {
      throw new Meteor.Error('Please select 1 option')
    }

    if(gameboard.swap2.step >= 5 && gameboard.swap2.action == 3 && gameboard.lastPlay != this.userId) {
      throw new Meteor.Error('Please select your solid')
    }

    let type;
    //In case player2 want to add 2 more stones
    if(gameboard.swap2.step < 5 && gameboard.swap2.action == Constant.SWAP2.ADD_2_MORE) {
      if(gameboard.lastPlay == this.userId) {
        throw new Meteor.Error('Not your turn')
      } else {
        type = gameboard.type.player2 
      }
    } 
    
    if (!gameboard.lastPlay && gameboard.player1 == this.userId) {
      if (gameboard.swap2.step == 2) {
        type = gameboard.type.player2
      }
    }
    if (gameboard && gameboard.lastPlay != this.userId && !gameboard.table[x][y].type) {
      type = type ? type : gameboard.player1 == this.userId ? gameboard.type.player1 : gameboard.type.player2
      gameboard.table[x][y].type = type
      gameboard.table[x][y].color = type == 'X' ? 'green' : 'red'
      gameboard.swap2.step = gameboard.swap2.step < 3 ? gameboard.swap2.step + 1 : gameboard.swap2.step
      if(gameboard.swap2.action == Constant.SWAP2.ADD_2_MORE) {
        gameboard.swap2.step = gameboard.swap2.step < 5 ? gameboard.swap2.step + 1 : gameboard.swap2.step
      }

      let lastPlay
      if(gameboard.swap2.action == Constant.SWAP2.PLAYING) {
        lastPlay = gameboard.swap2.step == 3 ? this.userId : null
      } else if(gameboard.swap2.action == Constant.SWAP2.ADD_2_MORE) {
        lastPlay = gameboard.swap2.step == 5 ? this.userId : gameboard.lastPlay
      } else {
        lastPlay = this.userId
      }
      
      if (checkWinner(x, y, type, gameboard.table)) {
        gameboard.winnerName = Meteor.user().username
        gameboard.winnerId = this.userId
        gameboard.status = 'finished'
      }
      GameBoards.update(gameboard._id, {
        $set: {
          table: gameboard.table,
          lastPlay,
          winnerName: gameboard.winnerName,
          winnerId: gameboard.winnerId,
          status: gameboard.status,
          swap2: {
            step: gameboard.swap2.step,
            action: gameboard.swap2.action
          }
        }
      })
    } else {
      throw new Meteor.Error('Not your turn')
    }

  },

  setAction(act) {
    act = Number(act)
    let gameboard = getBoard()
    let change = {
      swap2: {
        action: act,
        step: gameboard.swap2.step
      }
    }
    if(act == Constant.SWAP2.SWAP) {
      change.player1 = gameboard.player2
      change.player1Name = gameboard.player2Name
      change.player2 = gameboard.player1
      change.player2Name = gameboard.player1Name
      change.lastPlay = this.userId
    }
    GameBoards.update(gameboard._id, {
      $set: change
    })
  },

  selectSolid(solid) {
    let solid2 = solid == 'X' ? 'O' : 'X'
    let gameboard = getBoard()
    if(gameboard.player1 == this.userId && gameboard.type.player1 != solid) {
      gameboard.type.player1 = solid
      gameboard.type.player2 = solid2
      gameboard.lastPlay = gameboard.player1
    } else if(gameboard.player2 == this.userId && gameboard.type.player2 != solid) {
      gameboard.type.player2 = solid
      gameboard.type.player1 = solid2
      gameboard.lastPlay = gameboard.player2
    }
    gameboard.swap2.action = 4
    GameBoards.update(gameboard._id, {
      $set: {
        type: gameboard.type, 
        lastPlay: gameboard.lastPlay,
        swap2: gameboard.swap2
      }})
  },

  findBestMove() {
    let gameboard = getBoard()
    let type = gameboard.player1 == this.userId ? gameboard.type.player1 : gameboard.type.player2
    let bestInRow = findInRow(gameboard.table, type)
    let bestInCol = findInCol(gameboard.table, type)
    if(bestInCol.max > bestInRow.max) {
      return bestInCol.coods[0]
    } 
    return bestInRow.coods[0]
  },

  quitGame() {
    let gameboard = getBoard()
    // remove current user from game
    if (gameboard.player1 == Meteor.userId()) {
      gameboard.player1 = null
      gameboard.player1Name = null
    } else {
      gameboard.player2 = null
      gameboard.player2Name = null
    }

    // If both players has quit game, we destroy the table
    if (!gameboard.player1 && !gameboard.player2) {
      GameBoards.remove({ _id: gameboard._id })
    } else {
      GameBoards.update(gameboard._id, {
        $set: {
          player1: gameboard.player1,
          player1Name: gameboard.player1Name,
          player2: gameboard.player2,
          status: 'waiting',
          lastPlay: '',
          player2Name: gameboard.player2Name,
          table: grid()
        }
      })
    }
  }
})

/**
 * Create a table (19 x 19)
 */
function grid() {
  let rows = 19;
  let cols = 19;
  let table = []
  for (let i = 0; i < rows; i++) {
    let row = []
    for (let j = 0; j < cols; j++) {
      row.push({ type: '', color: '' })
    }
    table.push(row)
  }
  return table
}

function getBoard() {
  return GameBoards.find({ $or: [{ player1: Meteor.userId() }, { player2: Meteor.userId() }] }).fetch()[0]
}

function checkWinner(x, y, type, grid) {
  return checkDiagonalRight(x, y, type, grid) || checkCol(x, y, type, grid) || checkRow(x, y, type, grid) || checkDiagonalLeft(x, y, type, grid)
}

function checkRow(x, y, type, grid) {
  var left = 0
  var right = 0
  var i = y > 0 ? y - 1 : 0
  var j = y < 18 ? y + 1 : 0;
  while (i >= 0 && grid[x][i].type == type) {
    left++;
    i--;
  }
  while (j <= 18 && grid[x][j].type == type) {
    right++
    j++
  }

  // if ((left + right + 1) >= 5 && ((i >= 0 && grid[x][i].type == '') || (j <= 19 && grid[x][j].type == ''))) {
  //   return true
  // }

  return (left + right + 1) == 5
}

function checkCol(x, y, type, grid) {
  var top = 0
  var down = 0
  var i = x > 0 ? x - 1 : 0
  var j = x < 18 ? x + 1 : 0;
  while (i >= 0 && grid[i][y].type == type) {
    top++;
    i--;
  }
  while (j <= 18 && grid[j][y].type == type) {
    down++
    j++
  }

  // if ((top + down + 1) >= 5 && ((i >= 0 && grid[i][y].type == '') || (j <= 19 && grid[j][y].type == ''))) {
  //   return true
  // }
  return (top + down + 1) == 5
}

function checkDiagonalLeft(x, y, type, grid) {
  var top = 0
  var down = 0
  var iTop = x > 0 ? x - 1 : 0
  var jTop = y > 0 ? y - 1 : 0;
  var iBot = x < 18 ? x + 1 : 0
  var jBot = y < 18 ? y + 1 : 0
  while (iTop >= 0 && jTop >= 0 && grid[iTop][jTop].type == type) {
    top++;
    iTop--
    jTop--
  }
  while (iBot <= 18 && jBot && grid[iBot][jBot].type == type) {
    down++
    iBot++
    jBot++
  }

  // if ((top + down + 1) >= 5 && ((iTop >= 0 && jTop >=0 && grid[iTop][jTop].type == '') || (iBot <= 19 && jBot <= 19 && grid[iBot][jBot].type == ''))) {
  //   return true
  // }
  return (top + down + 1) == 5
}

function checkDiagonalRight(x, y, type, grid) {
  var top = 0
  var down = 0
  var iTop = x > 0 ? x - 1 : 0
  var jTop = y < 18 ? y + 1 : 0;
  var iBot = x < 18 ? x + 1 : 0
  var jBot = y > 0 ? y - 1 : 0
  while (iTop >= 0 && jTop >= 0 && grid[iTop][jTop].type == type) {
    top++;
    iTop--
    jTop++
  }
  while (iBot <= 18 && jBot && grid[iBot][jBot].type == type) {
    down++
    iBot++
    jBot--
  }

  // if ((top + down + 1) >= 5 && ((iTop >= 0 && jTop <= 19 && grid[iTop][jTop].type == '') || (iBot <= 19 && jBot >= 0 && grid[iBot][jBot].type == ''))) {
  //   return true
  // }
  return (top + down + 1) == 5
}

function findInRow(grid, type) {
  let max = 0
  let emptyls = []
  for (let i = 0; i < 19; i++) {
    let row = grid[i]
    let index = 0
    while (index <= 12) {
      //find in each block 5 elements
      let score = 0
      let ls = []
      for (let j = index; j <= index + 4; j++) {
        if (row[j].type == type) {
          score += 1
        } else if (row[j].type != type && row[j].type != '') {
          score -= 1
        } else {
          ls.push({ i, j })
        }
      }
      if (score > max) {
        max = score
        emptyls = ls
      }
      index++
    }
  }
  return {
    coods: emptyls,
    max
  }
}

function findInCol(grid, type) {
  let max = 0
  let emptyls = []
  for (let i = 0; i < 19; i++) {
    
    let index = 0
    while (index <= 12) {
      //find in each block 5 elements
      let score = 0
      let ls = []
      for (let j = index; j <= index + 4; j++) {
        
        if (grid[j][i].type == type) {
          score += 1
        } else if (grid[j][i].type != type && grid[j][i].type != '') {
          score -= 1
        } else {
          ls.push({ j, i })
        }
      }
      if (score > max) {
        max = score
        emptyls = ls
      }
      index++
    }
  }
  return {
    coods: emptyls,
    max
  }
}