import {Template} from 'meteor/templating'
import './board.html'
import { Meteor } from 'meteor/meteor';
import {Session} from 'meteor/session'

Template.board.helpers({
  userId() {
    return Meteor.userId()
  },

  timer() {
    
  }
})

Template.board.events({
  'click #gomoku'(event) {
    let id = event.target.id
    let pos = id.split('-')
    if(pos.length == 2) {
      Meteor.call('updateMove', pos[0], pos[1], (err) => {
        console.log(err)
        if(err) {
          alert(err.error)
        }
      })
    } 
  },

  'click .btn-quit'(event) {
      Meteor.call('quitGame', (err) => {

      })
  },
  'click .btn-help'() {
      Meteor.call('findBestMove', (err, data) => {
        console.log(err, data)
        document.getElementById(data.i+'-'+data.j).style.border = 'solid 2px red'
      })
  },

  'click .btn-option'(event) {
    let act = event.target.id
    Meteor.call('setAction', act, (err) => {
      console.log(err)
    })
  },
  'click .btn-solid'(event) {
    let solid = event.target.id
    Meteor.call('selectSolid', solid, (err) => {
      console.log(err)
    })
  }
})
