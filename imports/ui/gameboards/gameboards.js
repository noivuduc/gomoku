import {Template} from 'meteor/templating'
import {GameBoards} from '../../api/gameboard.js'
import {Session} from 'meteor/session'
import './gameboards.html'
import { Meteor } from 'meteor/meteor';

Template.gameboards.helpers({
  boards() {
    return GameBoards.find({$or: [{player1: null}, {player2: null}]})
  },
  currentGame() {
    return GameBoards.find({$or: [{player1: Meteor.userId()}, {player2:Meteor.userId()}]}).fetch()[0]
  }
})

Template.gameboards.events({

  'submit form'(event) {
    event.preventDefault()
    let name = event.target.gameboard.value
    if(!name) {
      return alert('Please enter the name')
    }
    Meteor.call('createGame', name, (err) => {
      console.log(err)
    })
  },
  'click .enter_board'(event) {
      let boardId = event.target.id
      Meteor.call('join', boardId, (err) => {
      })
  },
})