import {Template} from 'meteor/templating'
import './header.html'

Template.header.helpers({
  currentUser() {
    return Meteor.user()
  }
})