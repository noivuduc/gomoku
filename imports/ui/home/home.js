import {Template} from 'meteor/templating'
import './home.html'


Template.home.helpers({
  isAuth() {
    console.log(Meteor.user())
    // Meteor.logout()
    return !!Meteor.user()
  },
  processing: function() {
    return Meteor.loggingIn();
  }
})
Accounts.onLogin(function() {
  var path = FlowRouter.current().path;
  
  if (path === "/login") {
      FlowRouter.go("/");
  }
});

Template.home.events({
  'click .login'() {
    FlowRouter.go('/login')
  },
  'click .register'() {
    FlowRouter.go('/register')
  }
})