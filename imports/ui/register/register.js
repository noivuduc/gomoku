import {Template} from 'meteor/templating'
import './register.html'

Template.register.events({
  'submit form'(event) {
    event.preventDefault()
    event.preventDefault();
      var username = event.target.username.value;
      var password = event.target.password.value;

      Accounts.createUser({username, password}, function(err) {
          if (err) {
              console.log(err);
              alert(err.reason);
          } else {
            FlowRouter.go('/')
          }
      });
  }
})